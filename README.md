# dotfiles

## ZSH

Install [Oh My ZSH](https://ohmyz.sh/#install)
<pre>
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
</pre>

Copy `.zshrc`
<pre>
  cp .zshrc ~/.zshrc
</pre>

## Vim

Install [Vim Pathogen](https://github.com/tpope/vim-pathogen)
<pre>
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
</pre>

Install [ctrl-p](https://github.com/ctrlpvim/ctrlp.vim)
<pre>
$ cd ~/.vim
$ git clone https://github.com/ctrlpvim/ctrlp.vim.git bundle/ctrlp.vim
</pre>

Install [vividchalk theme](https://www.vim.org/scripts/script.php?script_id=1891)

Copy `.vimrc`
<pre>
cp .vimrc ~/.vimrc
</pre>
